function test(expected, result)
  if expected ~= result then
    print("FAILED " .. (expected or "nil") .. "!=" .. (result or "nil"))
  else
    print("SUCCEEDED")
  end
end

return test
