local lpeg = require("lpeg")
local pt = require("..pt")


local function node(tag, ...)
  local labels = table.pack(...)
  local params = table.concat(labels, ", ")
  local fields = string.gsub(params, "(%w+)", "%1 = %1")
  local code = string.format(
    "return function (%s) return {tag = '%s', %s} end",
    params,
    tag,
    fields
  )
  return load(code)()
end

local function nodeSeq(st1, st2)
  if st2 == nil then
    return st1
  else
    return { tag = "seq", st1 = st1, st2 = st2 }
  end
end

local function foldBin(lst)
  local tree = lst[1]
  for i = 2, #lst, 2 do
    tree = { tag = "binop", e1 = tree, op = lst[i], e2 = lst[i + 1] }
  end
  return tree
end

local function foldUn(un)
  return { tag = "unop", val = "minus", e1 = node("number", "val")(tonumber(un)) }
end

local function foldNot(exp)
  return { tag = "unop", val = "not", e1 = exp[1] }
end

--- grammar
local block_comment = lpeg.P("#{") * (lpeg.P(1) - "}#") ^ 0 * "}#"
local comment = block_comment + ("#" * (lpeg.P(1) - "\n") ^ 0)
local maxmatch = 0
local maxline = 0
local new_line = lpeg.P("\n") * lpeg.P(function(_, _p)
  maxline = maxline + 1
  return true
end) -- * space (needs to go to the grammar table)
local space = lpeg.V "space"
local alpha = lpeg.R("AZ", "az")
local digit = lpeg.R("09")
local alphanum = alpha + digit + "_"
-- 0[xX][0-9a-fA-F]+
local hexd = lpeg.R("09", "af", "AF")
local minus = lpeg.P("-") * ((lpeg.R("09") ^ 1 * ("." * lpeg.R("09")) ^ 0) / foldUn) * space
local hex = ("0" * lpeg.S("xX") * hexd * hexd * hexd * hexd)
local num = (((lpeg.R("09") ^ 1 * ("." * lpeg.R("09")) ^ 0)) + hex) * space / tonumber / node("number", "val")
local numeral = (minus + num) * space



local opA = lpeg.C(lpeg.S("-+")) * space
local opM = lpeg.C(lpeg.S("%*/")) * space
local opE = lpeg.C(lpeg.S("^")) * space
local opEq = lpeg.C(lpeg.P("<=") + ">=" + ">" + "<" + "==" + "!=") * space
local reserved = { "var", "function", "return", "elseif", "if", "else", "while", "and", "or", "new" }
local excluded = lpeg.P(false)
for i = 1, #reserved do
  excluded = excluded + reserved[i]
end
excluded = excluded * -alphanum
local ID = lpeg.V "ID"
local var = ID / node("variable", "var")


local function T(t)
  return t * space
end

local function Rw(t)
  assert(excluded:match(t))
  return t * -alphanum * space
end

local function foldIndex(lst)
  local tree = lst[1]
  for i = 2, #lst do
    tree = { tag = "indexed", array = tree, index = lst[i] }
  end
  return tree

end

-- TODO: THIS FUNCTION ONLY WORKS FOR
-- one dimension arrays
local function foldNew(lst)
  local tree = { tag = "new", size = lst[#lst], default = nil }
  if #lst > 1 then
    tree = { tag = "newmult", qty = #lst, sizes = {} }
    for i = 1, #lst do
      tree.sizes[i] = lst[i]
    end
  end
  return tree
end

local block = lpeg.V "block"
local funcDev = lpeg.V "funcDev"
local stat = lpeg.V "stat"
local stats = lpeg.V "stats"
local printStmt = lpeg.V "printStmt"
local call = lpeg.V "call"
local exp = lpeg.V "exp"
local factor = lpeg.V "factor"
local if1 = lpeg.V "if1"
local else1 = lpeg.V "else1"
local eq = lpeg.V "eq"
local or1 = lpeg.V "or1"
local params = lpeg.V "params"
local args = lpeg.V "args"
local g = lpeg.P {
  "prog", -- [1] = "prog"
  prog = space * lpeg.Ct(funcDev ^ 1) * -1,
  funcDev = ((Rw "function" * ID * T "(" * params * T ")" * block)
      + (Rw "function" * ID * T "(" * params * T ");")) / node("function", "name", "params", "body"),
  call = ID * T "(" * args * T ")" / node("call", "fname", "args"),
  params = lpeg.Ct((ID * (T "," * ID) ^ 0) ^ -1),
  args = lpeg.Ct((exp * (T "," * exp) ^ 0) ^ -1),
  stats = (T ";" ^ 0 * stat * space * (T ";" ^ 1 * stats * space) ^ -1 / nodeSeq) * T ";" ^ 0 * space,
  stat = space * printStmt
      + block
      + Rw "var" * ID * T "=" * exp / node("local", "name", "init")
      + Rw "var" * ID / node("local", "name")
      + Rw "while" * exp * block / node("while1", "cond", "body")
      + if1
      + call
      + lpeg.V "lhs" * T "=" * exp * space / node("assgn", "id", "exp")
      + Rw "return" * exp / node("ret", "exp"),
  if1 = Rw("if") * exp * block * else1 ^ -1 / node("if1", "cond", "th", "el"),
  else1 = (Rw("else") * block) +
      (Rw("elseif") * exp * block * (else1 ^ -1)) / node("if1", "cond", "th", "el"),
  printStmt = T "@" * exp * space / node("print", "exp"),
  block = T "{" * (stats * T ";" ^ -1) ^ 0 * T "}" / node("block", "body"),
  factor = lpeg.Ct(Rw "new" * (T "[" * exp * T "]") ^ 1) / foldNew
      + lpeg.V "notExp"
      + space * numeral
      + T "(" * exp * T ")"
      + call
      + lpeg.V "lhs",
  exponential = space * lpeg.Ct((factor * (opE * lpeg.V "exponential") ^ 0)) * space / foldBin,
  term = space * lpeg.Ct((lpeg.V "exponential" * (opM * lpeg.V "exponential") ^ 0)) * space / foldBin,
  add = space * lpeg.Ct((lpeg.V "term" * (opA * lpeg.V "term") ^ 0)) * space / foldBin,
  eq = space * lpeg.Ct((lpeg.V "add" * (opEq * lpeg.V "add") ^ 0)) * space / foldBin,
  exp = lpeg.V "and1" + or1 + eq,
  and1 = eq * Rw "and" * eq / node("and1", "exp1", "exp2"),
  or1 = eq * Rw "or" * eq / node("or1", "exp1", "exp2"),
  lhs = lpeg.Ct(var * (T "[" * exp * T "]") ^ 0) / foldIndex,
  notExp = space * lpeg.Ct((T "!" * factor)) * space / foldNot,
  space = ((comment + new_line + lpeg.S(" \t")) ^ 0 * lpeg.P(function(_, p)
    maxmatch = math.max(maxmatch, p)
    return true
  end)),
  ID = (lpeg.C(alpha * alphanum ^ 0) - excluded) * space
}
g = g * -1

----- frontend /parser ----

local function syntaxError(source, maxmatch, maxline)
  print("error on " .. maxline .. " line")
  print(maxmatch)
  print(string.sub(source, math.max(maxmatch - 5, 0), maxmatch) ..
    "| here |" .. string.sub(source, maxmatch + 1, math.min(maxmatch + 10, #source)))
end

local function parse(source)
  maxmatch = 0
  maxline = 0
  local res = g:match(source)
  if not res then
    syntaxError(source, maxmatch, maxline)
    error("error cannot complete the parsing")
  end
  return res
end

-- backend / compiler ---
local Compiler = {}

function Compiler:new()
  self.funcs = {}
  self.nvars = 0
  self.vars = {}
  self.locals = {}
  return self
end

function Compiler:var2index(id, op)
  local index = self.vars[id]
  if not index and op == "load" then
    error("cannot use variable before defining it")
  end
  if not index then
    index = self.nvars + 1
    self.nvars = index
    self.vars[id] = index
  end
  return index
end

function Compiler:addCode(op)
  local code = self.code
  code[#code + 1] = op
end

function Compiler:codeCall(ast)
  local func = self.funcs[ast.fname]
  if not func then
    error("func named " .. ast.fname .. " not defined")
  end
  if #func.params ~= #ast.args then
    error("wrong number of args " .. ast.fname)
  end
  for i = 1, #ast.args do
    self:codeExp(ast.args[i])
  end
  self:addCode("call")
  if #func.code > 1 then
    self:addCode(func.code)
  else
    self:addCode("willreplace")
    func.calls[#func.calls + 1] = { pos = self:currentPosition(), code = self.code }
  end
end

local ops = { ["+"] = "add", ["-"] = "sub", ["*"] = "mul", ["/"] = "div", ["%"] = "mod", ["^"] = "pow",
  [">="] = "ge", ["<="] = "le", ["=="] = "eq", ["!="] = "neq", [">"] = "g", ["<"] = "l" }
function Compiler:codeExp(ast)
  if ast.tag == "number" then
    self:addCode("push")
    self:addCode(ast.val)
  elseif ast.tag == "call" then
    self:codeCall(ast)
  elseif ast.tag == "variable" then
    local index = self:findLocal(ast.var)
    if index then
      self:addCode("loadL")
      self:addCode(index)
    else
      self:addCode("load")
      self:addCode(self:var2index(ast.var, "load"))
    end
  elseif ast.tag == "indexed" then
    self:codeExp(ast.array)
    self:codeExp(ast.index)
    self:addCode("getarray")
  elseif ast.tag == "new" then
    self:codeExp(ast.size)
    self:addCode("newarray")
  elseif ast.tag == "newmult" then
    for i = 1, #ast.sizes do
      self:codeExp(ast.sizes[i])
    end
    self:addCode("newmultarray")
    self:addCode(#ast.sizes)
  elseif ast.tag == "unop" then
    self:codeExp(ast.e1)
    self:addCode(ast.val)
  elseif ast.tag == "and1" then
    self:codeExp(ast.exp1)
    local jmp = self:codeJmp("jmpz")
    self:codeExp(ast.exp2)
    self:fixJmp2Here(jmp)
  elseif ast.tag == "or1" then
    self:codeExp(ast.exp1)
    local jmp = self:codeJmp("jmpone")
    self:codeExp(ast.exp2)
    self:fixJmp2Here(jmp)
  elseif ast.tag == "binop" then
    self:codeExp(ast.e1)
    self:codeExp(ast.e2)
    self:addCode(ops[ast.op])
  else error("invalid tree " .. pt.pt(ast))
  end
end

function Compiler:codeJmp(opCode)
  self:addCode(opCode)
  self:addCode(0)
  return self:currentPosition()
end

function Compiler:codeJmpB(opCode, label)
  self:addCode(opCode)
  self:addCode(label)
end

function Compiler:currentPosition()
  return #self.code
end

function Compiler:fixJmp2Here(jmp)
  self.code[jmp] = self:currentPosition()
end

function Compiler:codeAssgn(ast)
  local lhs = ast.id
  if lhs.tag == "variable" then
    self:codeExp(ast.exp)
    local index = self:findLocal(lhs.var)
    if index then
      self:addCode("storeL")
      self:addCode(index)
    else
      self:addCode("store")
      self:addCode(self:var2index(lhs.var, "store"))
    end
  elseif lhs.tag == "indexed" then
    self:codeExp(lhs.array)
    self:codeExp(lhs.index)
    self:codeExp(ast.exp)
    self:addCode("setarray")
  else
    error("unkown tag")
  end
end

function Compiler:findLocal(name)
  local loc = self.locals
  for i = #loc, 1, -1 do
    if name == loc[i] then
      return i
    end
  end
  for i = 1, #self.params do
    if self.params[i] == name then
      return -(#self.params - i)
    end
  end
  return nil
end

function Compiler:codeStmt(ast)
  if ast.tag == "assgn" then
    self:codeAssgn(ast)
  elseif ast.tag == "local" then
    if ast.init then
      self:codeExp(ast.init)
    else
      self:addCode(0)
    end
    self.locals[#self.locals + 1] = ast.name
  elseif ast.tag == "block" then
    self:codeBlock(ast)
  elseif ast.tag == "call" then
    self:codeCall(ast)
    self:addCode("pop")
    self:addCode(1)
  elseif ast.tag == "ret" then
    self:codeExp(ast.exp)
    self:addCode("ret")
    self:addCode(#self.locals + #self.params)
  elseif ast.tag == "seq" then
    self:codeStmt(ast.st1)
    self:codeStmt(ast.st2)
  elseif ast.tag == "print" then
    self:codeExp(ast.exp)
    self:addCode("print")
  elseif ast.tag == "while1" then
    local ilabel = self:currentPosition()
    self:codeExp(ast.cond)
    local jmp = self:codeJmp("jmpz")
    self:codeStmt(ast.body)
    self:codeJmpB("jmp", ilabel)
    self:fixJmp2Here(jmp)
  elseif ast.tag == "if1" then
    self:codeExp(ast.cond)
    local jmp = self:codeJmp("jmpz")
    self:codeStmt(ast.th)
    if ast.el == nil then
      self:fixJmp2Here(jmp)
    else
      local jmp2 = self:codeJmp("jmp")
      self:fixJmp2Here(jmp)
      self:codeStmt(ast.el)
      self:fixJmp2Here(jmp2)
    end
  end
end

function Compiler:codeFunction(ast)
  if not self.funcs[ast.name] then
    self.funcs[ast.name] = { code = {}, calls = {}, params = ast.params }
  end
  if ast.body or (ast.params and type(ast.params) ~= "table") then
    self.params = ast.params
    self.code = self.funcs[ast.name].code
    self:codeStmt(ast.body)
    self:addCode("push")
    self:addCode(0)
    self:addCode("ret")
    self:addCode(#self.locals + #self.params)
  end
end

function Compiler:codeBlock(ast)
  local oldLevel = #self.locals
  self:codeStmt(ast.body)
  local diff = #self.locals - oldLevel
  if diff > 0 then
    for i = 1, diff do
      table.remove(self.locals)
    end
    self:addCode("pop")
    self:addCode(diff)
  end
end

function Compiler:compile(ast)
  for i = 1, #ast do
    self:codeFunction(ast[i])
  end
  for fname, _f in pairs(self.funcs) do
    for index, _v in ipairs(self.funcs[fname].calls) do
      local call = self.funcs[fname].calls[index]
      call.code[call.pos] = self.funcs[fname].code
    end
  end
  if self.funcs["main"] == nil then
    error("there should be a main function")
  end
  return self.funcs["main"].code
end

------ virtual machine ----
local function ifTrue(cond)
  if cond then
    return 1
  else
    return 0
  end
end

local function printLang(item)
  if type(item) == 'table' then
    print("ARRAY " .. pt.pt(item))
  else
    print(item)
  end
end

local function multi_array(dimension, depth)
  if depth == #dimension then
    local currentDimension = dimension[depth]
    local array = { size = currentDimension }
    for i = 1, currentDimension do
      array[#array + 1] = { size = currentDimension } -- not sure
    end
    return array
  else
    local currentDimension = dimension[depth]
    local array = { size = currentDimension }
    for i = 1, currentDimension do
      array[#array + 1] = multi_array(dimension, depth + 1)
      array[#array].size = currentDimension
    end
    return array
  end

end

local function run(code, mem, stack, top_stack)
  local pc = 1
  local base = top_stack
  while true do
    if code[pc] == "ret" then
      local n = code[pc + 1]
      stack[top_stack - n] = stack[top_stack]
      return top_stack - n
    elseif code[pc] == "pop" then
      pc = pc + 1
      top_stack = top_stack - code[pc]
    elseif code[pc] == "call" then
      pc = pc + 1
      local code = code[pc]
      top_stack = run(code, mem, stack, top_stack)
    elseif code[pc] == "push" then
      pc = pc + 1
      top_stack = 1 + top_stack
      stack[top_stack] = code[pc]
    elseif code[pc] == "storeL" then
      pc = pc + 1
      local id = code[pc]
      stack[base + id] = stack[top_stack]
      top_stack = top_stack - 1
    elseif code[pc] == "store" then
      pc = pc + 1
      local id = code[pc]
      mem[id] = stack[top_stack]
      top_stack = top_stack - 1
    elseif code[pc] == "loadL" then
      pc = pc + 1
      local id = code[pc]
      top_stack = top_stack + 1
      stack[top_stack] = stack[base + id]
    elseif code[pc] == "load" then
      pc = pc + 1
      local id = code[pc]
      top_stack = 1 + top_stack
      stack[top_stack] = mem[id]
    elseif code[pc] == "print" then
      printLang(stack[top_stack])
      top_stack = top_stack - 1
    elseif code[pc] == "minus" then
      stack[top_stack] = -stack[top_stack]
    elseif code[pc] == "not" then
      stack[top_stack] = ifTrue(stack[top_stack] == 0) -- TODO only works for numerals
    elseif code[pc] == "add" then
      stack[top_stack - 1] = stack[top_stack - 1] + stack[top_stack]
      top_stack = top_stack - 1
    elseif code[pc] == "sub" then
      stack[top_stack - 1] = stack[top_stack - 1] - stack[top_stack]
      top_stack = top_stack - 1
    elseif code[pc] == "div" then
      stack[top_stack - 1] = stack[top_stack - 1] / stack[top_stack]
      top_stack = top_stack - 1
    elseif code[pc] == "mul" then
      stack[top_stack - 1] = stack[top_stack - 1] * stack[top_stack]
      top_stack = top_stack - 1
    elseif code[pc] == "mod" then
      stack[top_stack - 1] = stack[top_stack - 1] % stack[top_stack]
      top_stack = top_stack - 1
    elseif code[pc] == "pow" then
      stack[top_stack - 1] = stack[top_stack - 1] ^ stack[top_stack]
      top_stack = top_stack - 1
    elseif code[pc] == "g" then
      stack[top_stack - 1] = ifTrue(stack[top_stack - 1] > stack[top_stack])
      top_stack = top_stack - 1
    elseif code[pc] == "l" then
      stack[top_stack - 1] = ifTrue(stack[top_stack - 1] < stack[top_stack])
      top_stack = top_stack - 1
    elseif code[pc] == "ge" then
      stack[top_stack - 1] = ifTrue(stack[top_stack - 1] >= stack[top_stack])
      top_stack = top_stack - 1
    elseif code[pc] == "le" then
      stack[top_stack - 1] = ifTrue(stack[top_stack - 1] <= stack[top_stack])
      top_stack = top_stack - 1
    elseif code[pc] == "eq" then
      stack[top_stack - 1] = ifTrue(stack[top_stack - 1] == stack[top_stack])
      top_stack = top_stack - 1
    elseif code[pc] == "neq" then
      stack[top_stack - 1] = ifTrue(stack[top_stack - 1] ~= stack[top_stack])
      top_stack = top_stack - 1
    elseif code[pc] == "newarray" then
      local size = stack[top_stack]
      stack[top_stack] = { size = size }
    elseif code[pc] == "newmultarray" then
      pc = pc + 1
      local n_array = code[pc]
      local size = {}
      for i = 1, n_array do
        size[#size + 1] = stack[top_stack - i]
      end
      top_stack = (top_stack - n_array) + 1
      stack[top_stack] = multi_array(size, 1)
    elseif code[pc] == "getarray" then
      local array = stack[top_stack - 1]
      local index = stack[top_stack]
      if index >= array["size"] then
        error("array out of bounds")
      end
      top_stack = top_stack - 1
      stack[top_stack] = array[index]
    elseif code[pc] == "setarray" then
      local array = stack[top_stack - 2]
      local index = stack[top_stack - 1]
      local value = stack[top_stack]
      if index >= array["size"] then
        error("array out of bounds")
      end
      top_stack = top_stack - 3
      array[index] = value
    elseif code[pc] == "jmp" then
      pc = code[pc + 1]
    elseif code[pc] == "jmpz" then
      pc = pc + 1
      if stack[top_stack] == 0 then
        pc = code[pc]
      end
      top_stack = top_stack - 1
    elseif code[pc] == "jmpone" then
      pc = pc + 1
      if stack[top_stack] == 1 then
        pc = code[pc]
      end
      top_stack = top_stack - 1
    else error("non instruction " .. code[pc])
    end
    pc = pc + 1
  end
end

return function(input)
  --local input = io.read("a")
  --print(pt.pt(input))
  local ast = parse(input)
  --print(pt.pt(ast))
  local stack = {}
  local code = Compiler:new():compile(ast)
  --print(pt.pt(code))
  local mem = {}
  run(code, mem, stack, 0)
  --print(pt.pt(stack))
  --print(pt.pt(mem))
  --print(pt.pt(stack))
  return stack[1]
end
