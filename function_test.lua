local test = require 'test'
local g = require 'lang'


test(30, g("function main () {var a = 10; x = 20; return a + x;}"))
test(31, g("function b(t) { return t } function main () {var a = 10; x = 20; return b(1) + a + x}"))
test(2, g("function main() {a = new[2];@a; a[1] = 2;return a[1];}"))
test(2, g("function main() {a = new[2][2];@a; a[1][1] = 2;@a;return a[1][1];}"))
test(2, g("function main() {a = new[2][2];@a; a[1][1] = 2;@a;return a[1][1];} function abc() {return 1}"))
test(4, g("function abc() {return 1} function main() {a = new[2][2];@a; a[1][1] = 3;@a;return a[1][1] + abc();}"))
test(3, g("function abc() {return 1} function main() {abc(); a = new[2][2];@a; a[1][1] = 2;@a;return a[1][1] + abc();}"))
test(3,
  g("function abc(); function main() {abc(); a = new[2][2];@a; a[1][1] = 2;@a;return a[1][1] + abc();} function abc() { return 1}"))
test(2, g("function main() {a = new[2][2][2];@a; a[1][1][1] = 2;x = 1; return a[1][1][1]}"))
